import java.lang.Override;import java.lang.String; /**
 * Created by user on 13.12.2016.
 */
public class Transaction {
    private Account source;
    private Operation operation;
    private double amount;

    public Transaction(Account source, Operation operation, double amount) {
        this.source = source;
        this.operation = operation;
        this.amount = amount;
    }

    public Account getSource() {
        return source;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "source=" + source +
                ", operation=" + operation +
                ", amount=" + amount +
                '}';
    }
}

