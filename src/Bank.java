import java.lang.Double;import java.lang.Long;import java.lang.System;import java.util.*;import java.util.ArrayList;import java.util.Collections;import java.util.List;
/**
 * Created by user on 13.12.2016.
 */
public class Bank {
    private List<Client> clients = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();

    //Creating new account for existing client
    public void addAccount(Client client) {
        Account account = new Account(0);
        client.addAccount(account);
    }

    //Creating new client with new account
    public boolean addClient(Client client) {
        if (findClient(client) == null) {
            clients.add(client);
            addAccount(client);
            return true;
        }
        System.out.println("Given client already is a customer of the bank");
        return false;
    }

    //Searching for client
    public Client findClient(Client searchClient) {
        for (Client client : clients) {
            if (client.equals(searchClient)) return client;
        }
        return null;
    }

    //Withdrawing given amount form account
    public boolean withdrawFromAccount(Long accountNumber, Double amount) {
        Account account;
        if ((account = findAccount(accountNumber)) == null) {
            System.out.println("Incorrect account number");
            return false;
        }
        if (amount < 0) {
            System.out.println("Can't withdraw negative amount");
            return false;
        }
        if (amount > account.getBalance()) {
            System.out.println("Insufficient money. Can't withdraw");
            return false;
        }
        account.decreaseBalance(amount);
        Transaction transaction = new Transaction(account, Operation.WITHDRAW, amount);
        transactions.add(transaction);
        return true;
    }

    //Putting given amount to account
    public boolean putIntoAccount(Long accountNumber, Double amount) {
        Account account;
        if ((account = findAccount(accountNumber)) == null) {
            System.out.println("Incorrect account number");
            return false;
        }
        if (amount < 0) {
            System.out.println("Can't put negative amount");
            return false;
        }
        account.increaseBalance(amount);
        Transaction transaction = new Transaction(account, Operation.PUT, amount);
        transactions.add(transaction);
        return true;
    }

    //Return Account object by given long searchAccountNumber
    public Account findAccount(long searchAccountNumber) {
        for (Client client : clients)
            for (Account account : client.getAccounts()) {
                if (account.getAccountNumber() == searchAccountNumber) return account;
            }
        return null;
    }

    //Moving given amount from one account to another;
    public boolean moveFromOneAccountToAnother(Long sourceAccountNumber, Long destinationAccountNumber, Double amount) {
        boolean isWithdrawed = false;
        boolean isCashed = false;
        isWithdrawed = withdrawFromAccount(sourceAccountNumber, amount);
        if (isWithdrawed) {
            isCashed = putIntoAccount(destinationAccountNumber, amount);
        }
        if (isCashed) return true;
        putIntoAccount(sourceAccountNumber, amount);
        return false;
    }

    //Return list of account of given client
    public List<Account> findAccountsByClient(Client searchClient) {
        return searchClient.getAccounts();
    }

    //Printing list of clients
    public void printClients() {
        clients.forEach(System.out::println);
    }

    //Printing list of accounts sorted by account number
    public void printAccounts() {
        List<Account> accounts = new ArrayList<>();
        clients.forEach((client) -> {accounts.addAll(client.getAccounts());});
        Collections.sort(accounts);
        accounts.forEach(System.out::println);
    }

    //Printing list of client's accounts
    public void printAccountsForClient(Client client) {
        System.out.printf("Client %s %s has those accounts:%n", client.getFirstName(), client.getLastName());
        findAccountsByClient(client).forEach(System.out::println);
    }

    //Printing transaction list
    public void printTransactions() {
        transactions.forEach(System.out::println);
    }

    //Returning list of transaction of given account number
    public List<Transaction> findTransactionsByAccount(long searchAccountNumber) {
        List<Transaction> result = new ArrayList<>();
        transactions.forEach(transaction -> {if (transaction.getSource().getAccountNumber() == searchAccountNumber) result.add(transaction);});
        return result;
    }

    //Printing list of transaction of given account number
    public void printTransactionsForAccount(Long accountNumber) {
        System.out.printf("Transaction list for account: %d%n", accountNumber);
        findTransactionsByAccount(accountNumber).forEach(System.out::println);
    }

}


